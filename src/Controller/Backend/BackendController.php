<?php

namespace App\Controller\Backend;


use App\Controller\BaseController;

class BackendController extends BaseController
{
    public $_theme = 'backend';

    public function adminDashboard()
    {
        $this->denyAccessUnlessGranted('ROLE_USER');

        // or add an optional message - seen by developers
        $this->denyAccessUnlessGranted('ROLE_USER', null, 'User tried to access a page without having ROLE_USER');
    }
}