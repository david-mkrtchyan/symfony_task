<?php

namespace App\Controller\Backend;

use App\Entity\Statistic;
use App\Entity\Utils;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\PromotionProducts;
use App\Entity\Promotion;
use App\Form\Promotion1Type;
use App\Helper\ArrayHelper;
use App\Repository\PromotionRepository;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Route("/promotion")
 */
class PromotionController extends BackendController
{
    /**
     * @Route("/", name="promotion_index", methods={"GET"})
     */
    public function index(PromotionRepository $promotionRepository): Response
    {
        $condition = ['promotion_user'=> $this->getUser()];

        if($this->getUser()->hasRole('ROLE_SUPER_ADMIN')){
            $condition = [];
        }

        return $this->render('promotion/index.html.twig', [
            'promotions' => $promotionRepository->findBy($condition, ['date'=> 'DESC']),
        ]);
    }


    /**
     * @Route("/new", name="promotion_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $promotion = new Promotion();
        $form = $this->createForm(Promotion1Type::class, $promotion);
        $form->handleRequest($request);

        if ($form->isSubmitted())
        {
            $promotion->setPromotionUser($this->getUser());

            $request = $request->request;
            $promotion_product = $request->get('promotion_product');
            $product_exist = ArrayHelper::getValue($promotion_product, 'product_id.0');

            if(!isset($product_exist) || !$product_exist)
            {
                $form->addError(new FormError('You must add products'));
            }

            if($form->isValid())
            {
                $productIds = ArrayHelper::getValue($promotion_product, 'product_id');
                $note = ArrayHelper::getValue($promotion_product, 'note');

                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($promotion);


                foreach ($productIds as $key => $pro)
                {
                    $pro = abs(intval($pro));
                    if(!$pro)
                    {
                        continue;
                    }

                    $noteVal = ArrayHelper::getValue($note, $key);

                    $promProduct = new PromotionProducts();
                    $promProduct->setNote($noteVal);
                    $promProduct->setProductId($pro);
                    $promProduct->setPromotion($promotion);
                    $entityManager->persist($promProduct);
                }

                $entityManager->flush();

                $this->addFlash('success', 'The promotion has been created.');

                return $this->redirectToRoute('promotion_index');
            }

        }

        return $this->render('promotion/new.html.twig', [
            'promotion' => $promotion,
            'form' => $form->createView(),
            'values' => [],
        ]);
    }



    /**
     * @Route("/{id}", name="promotion_show", methods={"GET"})
     */
    public function show(Promotion $promotion): Response
    {
        return $this->render('promotion/show.html.twig', [
            'promotion' => $promotion,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="promotion_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Promotion $promotion): Response
    {
        $form = $this->createForm(Promotion1Type::class, $promotion);
        $form->handleRequest($request);


        if ($form->isSubmitted())
        {
            $request = $request->request;
            $promotion_product = $request->get('promotion_product');
            $product_exist = ArrayHelper::getValue($promotion_product, 'product_id.0');

            if(!isset($product_exist) || !$product_exist)
            {
                $form->addError(new FormError('You must add products'));
            }

            if($form->isValid())
            {
                $productIds = ArrayHelper::getValue($promotion_product, 'product_id');
                $note = ArrayHelper::getValue($promotion_product, 'note');

                foreach ($promotion->getPromotionProducts() as $val){
                    $promotion->removePromotionProduct($val);
                }

                $entityManager =  $this->getDoctrine()->getManager();

                foreach ($productIds as $key => $pro)
                {
                    $pro = abs(intval($pro));

                    if(!$pro ||  !intval($pro))
                    {
                        continue;
                    }

                    $noteVal = ArrayHelper::getValue($note, $key);

                    $promProduct = new PromotionProducts();
                    $promProduct->setNote($noteVal);
                    $promProduct->setProductId($pro);
                    $promProduct->setPromotion($promotion);
                    $entityManager->persist($promProduct);
                }

                $entityManager->flush();

                $this->addFlash('success', 'The promotion has been changed.');

                return $this->redirectToRoute('promotion_index');
            }

        }

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('promotion_index');
        }

        return $this->render('promotion/edit.html.twig', [
            'promotion' => $promotion,
            'form' => $form->createView(),
            'values' => $promotion->getPromotionProducts(),
        ]);
    }

    public function ajaxPromotionProducts(Request $request)
    {
        $entityManager = $this->getDoctrine()->getManager();

        if (!$request->isXmlHttpRequest()) {
            return new JsonResponse(array(
                'status' => 'Error',
                'message' => 'Error'),
                400);
        }

        if (isset($request->request)) {

            $promotion_id = $request->request->get('promotion_id');

            $promotion_id = intval($promotion_id);

            if ($promotion_id == 0)
            {
                // You can customize error messages
                // However keep in mind that this data is visible client-side
                // You shouldn't give out clues to what went wrong to potential attackers
                return new JsonResponse(array(
                    'status' => 'Error',
                    'message' => 'Error'),
                    400);
            }

            $promotionRepository = $entityManager->getRepository(Promotion::class);
            $promotion = $promotionRepository->findOneBy(array(
                'id' => $promotion_id,
//                'promotion_user' => $this->getUser()
            ));

            if ($promotion === null)
            {
                // Looks like something went wrong
                return new JsonResponse(array(
                    'status' => 'Error',
                    'message' => 'Error'),
                    400);
            }

            // All the test were perfomed, time to handle the data and perform the request
            // Which in our case means retriving the products for
            $products = $promotion->getPromotionProducts();
            // Transform data to whatever form is needed for the client side
            // In my case, I need an array with names and ids

            $products_array = array();
            $user = $this->getUser();

            foreach ($products as $product)
            {
                $productID = $product->getProductId();
                $arr=array(
                    'note' => $product->getNote(),
                    'id' => $productID,
                );


                if($user->hasRole('ROLE_SUPER_ADMIN'))
                {

                    $productRepository = $entityManager->getRepository(Promotion::class);
                    $productsClick = $productRepository::getClickByProductId($entityManager, $productID);

                    $arr['clicks'] = isset($productsClick[$product->getId()]) ? $productsClick[$product->getId()]['click'] : '';
                }

                array_push($products_array, $arr);
            }

            // Send all this back to client
            return new JsonResponse(array(
                'status' => 'OK',
                'role_admin' => $user->hasRole('ROLE_SUPER_ADMIN') ? true : false,
                'message' => $products_array),
                200);
        }
    }

    /**
     * @Route("/{id}", name="promotion_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Promotion $promotion): Response
    {
        if ($this->isCsrfTokenValid('delete' . $promotion->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($promotion);
            $entityManager->flush();
        }

        $this->addFlash('success', 'The promotion has been deleted.');

        return $this->redirectToRoute('promotion_index');
    }


}
