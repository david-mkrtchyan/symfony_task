<?php

namespace App\Controller\Backend;

use App\Entity\Promotion;
use App\Entity\PromotionProducts;
use App\Entity\Statistic;
use App\Entity\Utils;
use App\Form\Promotion1Type;
use App\Form\StatisticType;
use App\Helper\ArrayHelper;
use App\Repository\StatisticRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormError;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/statistic")
 */
class StatisticController extends BackendController
{

    /**
     * @Route("/", name="statistic_index", methods={"GET"})
     */
    public function index(StatisticRepository $statisticRepository): Response
    {
        return $this->render('statistic/index.html.twig', [
            'statistics' => $statisticRepository->findAll(),
        ]);
    }

    /**
     * @Route("/add", name="statistic_add", methods={"GET","POST"})
     */
    public function add(Request $request): Response
    {
        $statistic = new Statistic();
        $form = $this->createForm(StatisticType::class, $statistic);
        $form->handleRequest($request);

        if ($form->isSubmitted())
        {
            $entityManager = $this->getDoctrine()->getManager();

            $getStatistic = $request->request->get('statistic');
            $product_id = $getStatistic['product_id'];
            $promotionRepository = $entityManager->getRepository(PromotionProducts::class);
            $promotionProd = $promotionRepository->findBy(array(
                'product_id' => $product_id
            ),['id'=> 'DESC']);

            if(!$promotionProd){
                $form->addError(new FormError('Product id isn\'t exists'));
            }


            if($form->isValid())
            {
                $statistic->setStatisticUser($this->getUser());
                $entityManager->persist($statistic);
                $entityManager->flush();

                $this->addFlash('success', 'The statistic has been added.');

                return $this->redirectToRoute('promotion_index');
            }
        }

        return $this->render('statistic/new.html.twig', [
            'statistic' => $statistic,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="statistic_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Statistic $statistic): Response
    {
        if ($this->isCsrfTokenValid('delete'.$statistic->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($statistic);
            $entityManager->flush();
        }

        return $this->redirectToRoute('statistic_index');
    }

}
