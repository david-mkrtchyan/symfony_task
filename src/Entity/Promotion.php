<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PromotionRepository")
 * @ORM\Table(name="promotions")
 */
class Promotion
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=false)
     */
    private $title;

    /**
     * @ORM\Column(type="date")
     */
    private $date;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="promotion_user")
     * @ORM\JoinColumn(nullable=false,onDelete="CASCADE")
     */
    private $promotion_user;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\PromotionProducts", mappedBy="promotion", orphanRemoval=true)
     */
    private $promotionProducts;

    public function __construct()
    {
        $this->promotionProducts = new ArrayCollection();
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }


    public function getPromotionUser(): ?User
    {
        return $this->promotion_user;
    }

    public function setPromotionUser(?User $promotion_user): self
    {
        $this->promotion_user = $promotion_user;

        return $this;
    }

    /**
     * @return Collection|PromotionProducts[]
     */
    public function getPromotionProducts(): Collection
    {
        return $this->promotionProducts;
    }

    public function addPromotionProduct(PromotionProducts $promotionProduct): self
    {
        if (!$this->promotionProducts->contains($promotionProduct)) {
            $this->promotionProducts[] = $promotionProduct;
            $promotionProduct->setPromotion($this);
        }

        return $this;
    }

    public function removePromotionProduct(PromotionProducts $promotionProduct): self
    {
        if ($this->promotionProducts->contains($promotionProduct)) {
            $this->promotionProducts->removeElement($promotionProduct);
            // set the owning side to null (unless already changed)
            if ($promotionProduct->getPromotion() === $this) {
                $promotionProduct->setPromotion(null);
            }
        }

        return $this;
    }



}
