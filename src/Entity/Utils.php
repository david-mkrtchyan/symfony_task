<?php

namespace App\Entity;


class Utils
{
    public static function minDate($array)
    {
        $arr = $array[0];
        $min=$array[0]['pro_data'];
        foreach($array as $key=> $d)
        {
            if($d['pro_data'] < $min)
            {
                $min=$d;
                $arr=$array[$key];
            }
        }
        return $arr;
    }


    public static function getThemes()
    {
        return [
            'backend',
            'frontend',
        ];
    }
}
