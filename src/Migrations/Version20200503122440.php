<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200503122440 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE statistics_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE promotions_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE promotion_products_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE statistics (id INT NOT NULL, statistic_user_id INT NOT NULL, product_id INT NOT NULL, click_number INT NOT NULL, date DATE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_E2D38B2297C1E418 ON statistics (statistic_user_id)');
        $this->addSql('CREATE TABLE promotions (id INT NOT NULL, promotion_user_id INT NOT NULL, title VARCHAR(255) NOT NULL, date DATE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_EA1B3034FF1325AF ON promotions (promotion_user_id)');
        $this->addSql('CREATE TABLE promotion_products (id INT NOT NULL, promotion_id INT NOT NULL, note VARCHAR(255) DEFAULT NULL, product_id INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_75EEFE1E139DF194 ON promotion_products (promotion_id)');
        $this->addSql('ALTER TABLE statistics ADD CONSTRAINT FK_E2D38B2297C1E418 FOREIGN KEY (statistic_user_id) REFERENCES fos_user (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE promotions ADD CONSTRAINT FK_EA1B3034FF1325AF FOREIGN KEY (promotion_user_id) REFERENCES fos_user (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE promotion_products ADD CONSTRAINT FK_75EEFE1E139DF194 FOREIGN KEY (promotion_id) REFERENCES promotions (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE promotion_products DROP CONSTRAINT FK_75EEFE1E139DF194');
        $this->addSql('DROP SEQUENCE statistics_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE promotions_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE promotion_products_id_seq CASCADE');
        $this->addSql('DROP TABLE statistics');
        $this->addSql('DROP TABLE promotions');
        $this->addSql('DROP TABLE promotion_products');
    }
}
