<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200505060017 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE promotions DROP CONSTRAINT FK_EA1B3034FF1325AF');
        $this->addSql('ALTER TABLE promotions ADD CONSTRAINT FK_EA1B3034FF1325AF FOREIGN KEY (promotion_user_id) REFERENCES fos_user (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE promotion_products DROP CONSTRAINT FK_75EEFE1E139DF194');
        $this->addSql('ALTER TABLE promotion_products ADD CONSTRAINT FK_75EEFE1E139DF194 FOREIGN KEY (promotion_id) REFERENCES promotions (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE promotions DROP CONSTRAINT fk_ea1b3034ff1325af');
        $this->addSql('ALTER TABLE promotions ADD CONSTRAINT fk_ea1b3034ff1325af FOREIGN KEY (promotion_user_id) REFERENCES fos_user (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE promotion_products DROP CONSTRAINT fk_75eefe1e139df194');
        $this->addSql('ALTER TABLE promotion_products ADD CONSTRAINT fk_75eefe1e139df194 FOREIGN KEY (promotion_id) REFERENCES promotions (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }
}
