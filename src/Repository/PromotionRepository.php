<?php

namespace App\Repository;

use App\Entity\Promotion;
use App\Entity\PromotionProducts;
use App\Entity\Statistic;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Promotion|null find($id, $lockMode = null, $lockVersion = null)
 * @method Promotion|null findOneBy(array $criteria, array $orderBy = null)
 * @method Promotion[]    findAll()
 * @method Promotion[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PromotionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Promotion::class);
    }


    public static function getClickByProductId($entityManager, $productId )
    {

        $statisticRepository = $entityManager->getRepository(Statistic::class);
        $statisticsAll = $statisticRepository->findBy(array(
            'product_id' => $productId,
        ),['date'=> 'DESC', 'id' => 'DESC']);

        $productRepository = $entityManager->getRepository(PromotionProducts::class);
        $productsAll = $productRepository
            ->createQueryBuilder('od')
            ->select('od')
            ->innerJoin(Promotion::class, 'pp', 'with', 'od.promotion = pp.id')
            ->where('od.product_id = :productId')
         //  ->andWhere('pp.promotion_user = :userId')
            ->setParameter('productId', $productId)
         //   ->setParameter('userId', $user)
            ->orderBy('pp.id', 'DESC')
            ->orderBy('pp.date', 'DESC')
            ->getQuery()
            ->getResult();

        $newProdList = [];
        foreach ($productsAll as $prod)
        {
            $ar = [
                'date' => $prod->getPromotion()->getDate()->format('Y-m-d'),
                'product_id' => $productId,
                'id' => $prod->getId(),
            ];
            $newProdList[] = $ar;
        }


        $lastQueryList = [];
        foreach ($statisticsAll as $stat_key => $stat)
        {
            $date_stat =  $stat->getDate()->format('Y-m-d');
            foreach ($newProdList as $key => $pos){
                if($date_stat >= $pos['date']){
                    $lastQueryList[$pos['id']] = [
                        'click' =>   $stat->getClickNumber(),
                        'date_stat' =>  $date_stat,
                        'date' =>  $pos['date'],
                    ];
                    unset($newProdList[$key]);
                    unset($statisticsAll[$stat_key]);
                    break;
                }
            }
        }

        return $lastQueryList;
    }

//    public function findAll()
//    {
//        return $this->findBy(array(), array( 'date' => 'DESC','id'=>'DESC',));
//    }

    // /**
    //  * @return Promotion[] Returns an array of Promotion objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Promotion
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
