<?php
namespace App\Twig\Functions;

use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class RouteExtension extends AbstractExtension
{
    public function getFunctions()
    {
        return [
            new TwigFunction('routeActive', [$this, 'routeActive']),
        ];
    }

    public function routeActive($route, $routeToCheck)
    {
        if (strpos($route, $routeToCheck) !== false) {
            return 'active';
        }
    }
}
?>